#!/bin/sh
fpm -n tuxblocs \
-s dir \
-t deb \
--name tuxblocs \
--license GPL \
--version 4.0.stable \
--architecture all \
--depends tix \
--description "Tuxblocs est un logiciel qui permet de représenter les nombres sous forme de blocs de base de numération." \
--url "https://forge.apps.education.fr/educajou/tuxblocs]https://forge.apps.education.fr/educajou/tuxblocs" \
--maintainer "Arnaud Champollion" \
linux/tuxblocs.desktop=/usr/share/applications/tuxblocs.desktop \
images/icone.png=/usr/share/pixmaps/tuxblocs.png \
linux/tuxblocs=/usr/bin/tuxblocs \
dist/tuxblocs=/usr/share
